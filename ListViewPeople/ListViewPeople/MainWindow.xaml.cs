﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ListViewPeople
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        const string DataFileName = @"..\..\person.txt";

        List<Person> peopleList = new List<Person>();


        public MainWindow()
        {
            InitializeComponent();
            LoadDataFromFile();
        }

        private void LoadDataFromFile()
        {
            // read the file
            if (File.Exists(DataFileName))
            {
                string[] peopleInfo = File.ReadAllLines(DataFileName);

                foreach (string personLine in peopleInfo)
                {
                    string[] person = personLine.Split(';');
                    string name = person[0];
                    int age;
                    if (!int.TryParse(person[1], out age))
                    {
                        MessageBox.Show("Error: reading file.");
                    }
                    Person p = new Person(name, age);
                    peopleList.Add(p);
                }
            }
            // populate a List<Person>
            // assign this list as itemsource
            lvPeople.ItemsSource = peopleList;
        }


        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnDeletePerson.IsEnabled = true;
            btnUpdatePerson.IsEnabled = true;

            var selectedPerson = lvPeople.SelectedItem; //SelectedItem is an object; selectedPerson is an object
            // casting selectedPerson to Person object type (*******polymorphism)
            if (selectedPerson is Person)
            {
                Person person = (Person)selectedPerson;
                txtName.Text = person.Name;
                txtAge.Text = person.Age.ToString();
            }

        }
        private void btnUpdatePerson_Click(object sender, RoutedEventArgs e)
        {
            if (lvPeople.SelectedIndex == -1)
            {
                MessageBox.Show("You need to choose one person");
                return;
            }
            string newName = txtName.Text;
            string newAge = txtAge.Text;

            Person pTobeUpdated = (Person)lvPeople.SelectedItem;
            pTobeUpdated.Name = newName;
            pTobeUpdated.Age = int.Parse(newAge);

            ResetValue();

            /*lvPeople.Items.Refresh();
            txtName.Clear();
            txtAge.Clear();
            lvPeople.SelectedIndex = -1;*/
        }

        private void btnDeletePerson_Click(object sender, RoutedEventArgs e)
        {
            if (lvPeople.SelectedIndex == -1)
            {
                MessageBox.Show("You need to choose one person");
                return;
            }

            Person pTobeDeleted = (Person)lvPeople.SelectedItem;
            peopleList.Remove(pTobeDeleted);

            ResetValue();

            /*lvPeople.Items.Refresh();
            txtName.Clear();
            txtAge.Clear();
            lvPeople.SelectedIndex = -1;*/
        }

        private void btnAddPerson_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            if (name.Length == 0)
            {
                MessageBox.Show("Enter a name", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                string ageText = txtAge.Text;
                int age;
                if (!int.TryParse(ageText, out age))
                {
                    MessageBox.Show("Enter value number for age", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                Person p = new Person(name, age);
                peopleList.Add(p);
            }
            
            

            ResetValue();
            /*lvPeople.Items.Refresh();
            txtName.Clear();
            txtAge.Clear();
            lvPeople.SelectedIndex = -1;*/
        }

        private void ResetValue()
        {
            lvPeople.Items.Refresh();
            txtName.Clear();
            txtAge.Clear();
            lvPeople.SelectedIndex = -1;
            btnDeletePerson.IsEnabled = false;
            btnUpdatePerson.IsEnabled = false;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //save list into the file
            using (StreamWriter writer = new StreamWriter(DataFileName))
            {
                foreach (Person p in peopleList)
                {
                    writer.WriteLine(p.toDataString());
                }
            }


        }

    }
}

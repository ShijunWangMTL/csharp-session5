using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ListViewPeople
{
    /// <summary>
    /// Interaction logic for GridViewWindow.xaml
    /// </summary>
    public partial class GridViewWindow : Window
    {

        const string DataFileName = @"..\..\person.txt";

        List<Person> peopleList = new List<Person>();


        private void LoadDataFromFile()
        {
            // read the file
            if (File.Exists(DataFileName))
            {
                string[] peopleInfo = File.ReadAllLines(DataFileName);

                foreach (string personLine in peopleInfo)
                {
                    string[] person = personLine.Split(';');
                    string name = person[0];
                    int age;
                    if (!int.TryParse(person[1], out age))
                    {
                        MessageBox.Show("Error: reading file.");
                    }
                    Person p = new Person(name, age);
                    peopleList.Add(p);
                }
            }
            // populate a List<Person>
            // assign this list as itemsource
            lvGridPeople.ItemsSource = peopleList;
        }


        public GridViewWindow()
        {
            InitializeComponent();
            LoadDataFromFile();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void lvGridPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnDeletePerson.IsEnabled = true;
            btnUpdatePerson.IsEnabled = true;

            var selectedPerson = lvGridPeople.SelectedItem; //SelectedItem is an object; selectedPerson is an object
            // casting selectedPerson to Person object type (*******polymorphism)
            if (selectedPerson is Person)
            {
                Person person = (Person)selectedPerson;
                txtName.Text = person.Name;
                txtAge.Text = person.Age.ToString();
            }

        }
        private void btnUpdatePerson_Click(object sender, RoutedEventArgs e)
        {
            if (lvGridPeople.SelectedIndex == -1)
            {
                MessageBox.Show("You need to choose one person");
                return;
            }
            string newName = txtName.Text;
            string newAge = txtAge.Text;

            Person pTobeUpdated = (Person)lvGridPeople.SelectedItem;
            pTobeUpdated.Name = newName;
            pTobeUpdated.Age = int.Parse(newAge);

            ResetValue();
        }

        private void btnDeletePerson_Click(object sender, RoutedEventArgs e)
        {
            if (lvGridPeople.SelectedIndex == -1)
            {
                MessageBox.Show("You need to choose one person");
                return;
            }

            Person pTobeDeleted = (Person)lvGridPeople.SelectedItem;
            peopleList.Remove(pTobeDeleted);

            ResetValue();
        }

        private void btnAddPerson_Click(object sender, RoutedEventArgs e)
        {
            string name = txtName.Text;
            string ageText = txtAge.Text;

            int age;
            if (!int.TryParse(ageText, out age))
            {
                MessageBox.Show("Enter value number for age", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Person p = new Person(name, age);
            peopleList.Add(p);

            ResetValue();
        }

        private void ResetValue()
        {
            lvGridPeople.Items.Refresh();
            txtName.Clear();
            txtAge.Clear();
            lvGridPeople.SelectedIndex = -1;
            btnDeletePerson.IsEnabled = false;
            btnUpdatePerson.IsEnabled = false;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //save list into the file
            using (StreamWriter writer = new StreamWriter(DataFileName))
            {
                foreach (Person p in peopleList)
                {
                    writer.WriteLine(p.toDataString());
                }
            }


        }
    }
}

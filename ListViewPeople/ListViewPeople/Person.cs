﻿using System;
using System.Windows;

namespace ListViewPeople
{

    public class InvalidDataException : Exception
    {

    }

    class Person
    {
        private string _name;
        private int _age;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public override string ToString()
        {
            return $"{Name} is {Age} years old";
        }

        public string toDataString()
        {
            return $"{Name};{Age}";
        }

    }
}

﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;



namespace ListViewTodoList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {

        const string DataFileName = @"..\..\todo.txt";

        List<Todo> todoList = new List<Todo>();

    private void LoadDataFromFile()
        {
            // read the file
            if (File.Exists(DataFileName))
            {
                string[] todoTxt = File.ReadAllLines(DataFileName);

                foreach (string todoLine in todoTxt)
                {
                    Todo todo = new Todo();
                    string[] todoItem = todoLine.Split(';');
                    todo.Task = todoItem[0];

                    string difficultyStr = todoItem[1];
                    int difficulty;
                    if (!int.TryParse(difficultyStr, out difficulty))
                    {
                        MessageBox.Show("Error: Invalid value from the file: " + todoLine);
                    }
                    else if (difficulty < 1 || difficulty > 5)
                    {
                        MessageBox.Show("Error: Invalid value from the file: " + todoLine);
                    }
                    else
                    {
                        todo.Difficulty = difficulty;
                    }

                    DateTime dt;
                    if (!DateTime.TryParse(todoItem[2], out dt))
                    {
                        MessageBox.Show("Error: Invalid value from the file: " + todoLine);                      
                    }
                    todo.Due = dt.Date.ToString("d");
                   
                    string status = todoItem[3];
                    string[] StatusOptions = { "Not started", "In progress", "Pending", "Completed", "On hold" };
                    for (int i = 0; i < StatusOptions.Length; i++)
                    {
                        if (status.Equals(StatusOptions[i]))
                        {
                            todo.Status = StatusOptions[i];
                        }
                    }
                    if (todo.Status == null)
                    {
                        MessageBox.Show("Error: Invalid value from the file: " + todoLine);
                    }

                    todoList.Add(todo);
                }
            }
            // populate a List<Person>
            // assign this list as itemsource
            lvGridTodo.ItemsSource = todoList;
        }




        public MainWindow()
        {
            InitializeComponent();
            LoadDataFromFile();          
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Todo tdTobeAdded = new Todo();

            if (txtTask.Text.Equals(""))
            {
                MessageBox.Show("Please enter the task.");
                return;
            }
            tdTobeAdded.Task = txtTask.Text;

            tdTobeAdded.Difficulty = (int)sliderDifficulty.Value;

            DateTime? selectedDate = dueDatePicker.SelectedDate;
            if (selectedDate == null)
            {
                MessageBox.Show("Please choose a date.");
                return;
            }
            tdTobeAdded.Due = selectedDate.Value.ToString("yyyy-MM-dd");

            tdTobeAdded.Status = cmbStatus.Text;

            todoList.Add(tdTobeAdded);

            ResetView();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lvGridTodo.SelectedIndex == -1)
            {
                MessageBox.Show("Please select one to-do item.");
                return;
            }

            Todo tdTobeDeleted = (Todo)lvGridTodo.SelectedItem;
            if (tdTobeDeleted != null)
            {
                MessageBoxResult result = MessageBox.Show("Are you sure to delete this task?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Error);
                if (result == MessageBoxResult.Yes)
                {
                    todoList.Remove(tdTobeDeleted);
                }
            }
            
            ResetView();

        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lvGridTodo.SelectedIndex == -1)
            {
                MessageBox.Show("Please select one to-do item.");
                return;
            }

            Todo tdTobeUpdated = (Todo)lvGridTodo.SelectedItem;

            if (txtTask.Text.Equals(""))
            {
                MessageBox.Show("Please enter the task.");
                return;
            }
            tdTobeUpdated.Task = txtTask.Text;

            tdTobeUpdated.Difficulty = (int)sliderDifficulty.Value;

            DateTime? selectedDate = dueDatePicker.SelectedDate;
            if (selectedDate == null)
            {
                MessageBox.Show("Please choose a date.");
                return;
            }
            tdTobeUpdated.Due = selectedDate.Value.ToString("yyyy-MM-dd");

            tdTobeUpdated.Status = cmbStatus.Text;

            ResetView();           
        }

        
       
        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            //display a saveFileDialog so user can save the file
            //assigned to Button2.
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.Title = "Save all the todo in a File";
            if (saveFileDialog1.ShowDialog() == true)
            {
                string allData = "";
                foreach (Todo td in todoList)
                {
                    allData += td.ToString() + "\n";
                }
                File.WriteAllText(saveFileDialog1.FileName, allData);
            }
            else
            {
                MessageBox.Show("File error", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }

        private void lvGridTodo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnDelete.IsEnabled = true;
            btnUpdate.IsEnabled = true;

            var selectedTodo = lvGridTodo.SelectedItem;
            if (selectedTodo is Todo)
            {
                Todo td = (Todo)selectedTodo;
                txtTask.Text = td.Task;
                sliderDifficulty.Value = td.Difficulty;
                dueDatePicker.SelectedDate = DateTime.Parse(td.Due);
                cmbStatus.Text = td.Status;
            }
        }

        private void ResetView()
        {
            lvGridTodo.Items.Refresh();

            txtTask.Clear();
            sliderDifficulty.Value = 1;
            dueDatePicker.SelectedDate = null;
            cmbStatus.Text = "Not started";

            lvGridTodo.SelectedIndex = -1;

            btnDelete.IsEnabled = false;
            btnUpdate.IsEnabled = false;
            
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //save list into the file
            try
            {
                using (StreamWriter writer = new StreamWriter(DataFileName))
                {
                    foreach (Todo t in todoList)
                    {
                        writer.WriteLine(t.toDataString());
                    }
                }

            }
            catch (IOException exc)
            {
                MessageBox.Show(exc.Message); ;
            }

        }

    }
}

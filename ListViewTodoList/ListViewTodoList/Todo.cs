﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListViewTodoList
{
    class Todo
    {
        private string _task;
        public string Task
        {
            get { return _task; }
            set { _task = value; }
        }

        private int _difficulty;
        public int Difficulty
        {
            get { return _difficulty; }
            set
            {
                _difficulty = value;
            }
        }

        private string _due;
        public string Due
        {
            get { return _due; }
            set { _due = value; }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
            }
        }

        public Todo(string task, int difficulty, string due, string status)
        {
            Task = task;
            Difficulty = difficulty;
            Due = due;
            Status = status;
        }

        public Todo()
        {

        }

        public override string ToString()
        {
            return $"{Task};{Difficulty};{Due};{Status}";
        }

        public string toDataString()
        {
            return $"{Task};{Difficulty};{Due};{Status}";
        }

        string[] StatusOptions = { "Not started", "In progress", "Pending", "Completed", "On hold" };




    }
}
